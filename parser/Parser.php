<?php

namespace parser;

use parser\src\{Request, Dom};

class Parser
{
    /**
     * @param string $method
     * @param string $url
     * @param array $options
     *
     * @return \parser\src\DomNode | false
     */
    public function request($method = 'GET', $url, $options = [])
    {
        try{
            $content = new Request($method, $url, $options);

            $dom = new Dom($content);

            return $dom->load();
        }catch (\Exception $e){
            echo "Error:" . $e->getMessage();
            return false;
        }
    }
}