<?php

namespace parser\src;

use parser\src\Interfaces\RequestInterface;

class Request implements RequestInterface
{
    /** @var string */
    private $method;

    /** @var string */
    private $request;

    /** @var string */
    private $uri;

    /**
     * @param string $method HTTP method
     * @param string $url
     * @param array $headers Request headers
     * @param string|null $body Request body
     */
    public function __construct($method, $url, array $headers = [], $body = null)
    {
        $this->method = $method;
        $this->uri = $url;
        $this->setHeaders($headers);
    }

    private function setHeaders(array $headers)
    {
        //processing headers  ...
    }

    /**
     * @return string
     */
    public function getMethod() :string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUri() :string
    {
        return $this->uri;
    }

    /**
     *
     * @throws \Exception file is not exists or wrong in uri
     *
     * @return string
     */
    public function request() :string
    {
        $this->processingUri();

        try{
            $content = file_get_contents($this->request);

            return $content;
        }catch (\Exception $e){

            throw new \Exception(sprintf('File is not exists or wrong in uri on line %s file %s'),
                $e->getLine(), $e->getFile());
        }
    }

    /**
     *
     */
    private function processingUri()
    {
        //do something with uri and assign value to request
        $this->request = $this->uri;
    }
}