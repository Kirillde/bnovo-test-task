<?php

namespace parser\src;

use parser\src\Interfaces\DomNodeInterface;

class DomNode implements DomNodeInterface
{
    /**
     * @var string $str
     */
    private $str;

    /**
     * @var array $nodes
     */
    private $nodes;

    public function __construct($str)
    {
       $this->str = $str;
    }

    /**
     * @throws \Exception Tag is not exists
     *
     * @return array
     */
    public function generateNodes() :array
    {
        /*
         * generate nodes and adding to massive
         * !nodes is tags of DOM with child tags inside!
         */
        $nodes = [];

        try{
            if(empty($nodes)) {
                throw new \Exception;
            }
            $this->nodes = $nodes;
            return $this->nodes;
        }catch(\Exception $e)
        {
            throw new \Exception(sprintf('File have not html tags inside on line %s file %s'),
                $e->getLine(), $e->getFile());
        }
    }

    /**
     * @param string $class
     *
     * @return array
     * @throws \Exception Tag is not exists
     */
    public function findTagByClass(string $class) :array
    {
        //with help cycle for or foreach processing and finding necessary element

        try{
            if(empty($tag)) {
                throw new \Exception;
            }

            return [];
        }catch(\Exception $e)
        {
            throw new \Exception(sprintf('Tag is not exists on line %s file %s'),
                $e->getLine(), $e->getFile());
        }
    }

    /**
     * @return array
     */
    public function getReviews() :array
    {
        $reviewByClass = $this->findTagByClass('review_list');

        /*
         * with help cycle for or foreach processing and finding necessary element
         * and return ready array with reviews
         *
         * in processing during cycle can using method $this->getChildTags($reviewByClass)
         */

        return [];
    }

    /**
     * This method find all child tags of parent
     *
     * @param string $parent
     * @return array
     */
    public function getChildTags(string $parent) :array
    {
        return [];
    }
}