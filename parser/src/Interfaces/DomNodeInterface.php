<?php

namespace parser\src\Interfaces;

interface DomNodeInterface
{
    /**
     * @return array
     */
    public function generateNodes() :array;
}

