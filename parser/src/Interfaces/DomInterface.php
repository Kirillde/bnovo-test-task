<?php

namespace parser\src\Interfaces;

interface DomInterface
{
    /**
     * This method make preparation of str and parsing
     * @return \parser\src\DomNode
     */
    public function load();
}

