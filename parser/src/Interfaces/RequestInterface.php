<?php

namespace parser\src\Interfaces;

interface RequestInterface
{
    /**
     * @return string
     */
    public function getMethod() :string;

    /**
     * @return string
     */
    public function getUri() :string;

    /**
     *
     * @throws \Exception file is not exists or wrong in uri
     *
     * @return string
     */
    public function request() :string;
}

