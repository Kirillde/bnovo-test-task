<?php

namespace parser\src;

use parser\src\Interfaces\DomInterface;
use parser\src\DomNode;

class Dom implements DomInterface
{
    /**
     * @var string $str
     */
    private $str;

    public function __construct($str)
    {
       $this->str = $str;
    }

    /**
     * This method make preparation of str and parsing
     * @return \parser\src\DomNode
     */
    public function load()
    {
        $this->prepare();

        return $this->parse();
    }

    /**
     * This method make preparation
     *
     * @return string
     */
    private function prepare() :string
    {
        //do something with str
        return $this->str;
    }

    /**
     * @return \parser\src\DomNode
     */
    private function parse()
    {
        $node = new DomNode($this->str);

        $node->generateNodes();

        return $node;
    }
}